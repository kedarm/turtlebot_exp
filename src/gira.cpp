 
#include "ros/ros.h"   //Librería de funciones del sistema ROS
#include "geometry_msgs/Twist.h"   //Librería "Twist" del package "geometry_msgs"
 
int main(int argc, char **argv)
{
 
	ros::init(argc, argv, "gira");   //Creación del nodo "gira"
 
        ros::NodeHandle n;
 
        ros::Publisher vel_pub_=n.advertise<geometry_msgs::Twist>("cmd_vel", 1);   //Publicación del topic "cmd_vel"
 
        geometry_msgs::Twist vel;
 
        ros::Rate loop_rate(10);   //Frecuencia de realización del bucle (10 Hz)
 
        while (ros::ok())   //Bucle mientras no se reciba "Ctrl+C"
        {
	      vel.linear.x = 0.2;   //velocidad de avance
       	      vel.angular.z = 0.4;   //velocidad de giro
 
	      vel_pub_.publish(vel);
 
              loop_rate.sleep();
        }
 
        return 0;
}
