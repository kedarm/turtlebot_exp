 
#include "ros/ros.h"   //Librería de funciones del sistema ROS
#include "geometry_msgs/Twist.h"   //Librería "Twist" del package "geometry_msgs"
#include "turtlebot_node/TurtlebotSensorState.h"   //Librería "TurtlebotSensorState" del package "turtlebot_node"
 
geometry_msgs::Twist vel;   //Declaración de la variable global vel
double alea;   //Declaración de la variable global alea
 
void callback(const turtlebot_node::TurtlebotSensorState& sen)   //Función de llamda con cada dato recibido
{	
        ros::NodeHandle n;
 
  	ros::Publisher vel_pub_=n.advertise<geometry_msgs::Twist>("cmd_vel", 1);   //Publicación del topic "cmd_vel"
 
  	ros::Rate loop_rate(10);   //Frecuencia de realización del bucle (10 Hz)
 
	ros::Time ahora;   //Declaración de variable tipo tiempo, variable ROS
 
 
  	if (sen.bumps_wheeldrops == 0)   //Comprobación si ha habido señal del parachoques
  	{
	        ::vel.linear.x = 0.2;
		::vel.angular.z = 0.0;
 
		vel_pub_.publish(::vel);
	}
  	else
  	{
		ahora = ros::Time::now();   //Asignación del tiempo actual a la variable ahora
 
 
		while (ros::Time::now() < (ahora + ros::Duration(0.5)))   //Bucle durante 0,5 segundos, retroceso después de choque
		{
			::vel.linear.x = -0.2;
			::vel.angular.z = 0.0;
 
			vel_pub_.publish(::vel);
 
			loop_rate.sleep();
		}
 
		::alea = ((rand() %40) -20)/10;   //Asignación de valor aleatorio
 
		while (::alea == 0)   //Bucle mientras la variable aleatoria sea cero
		{
			::alea = ((rand() %40) -20)/10;	
		}
 
		ahora = ros::Time::now();   //Asignación del tiempo actual a la variable ahora
 
		while (ros::Time::now() < (ahora + ros::Duration(1.5)))   //Bucle durante 1,5 segundos, giro de ángulo aleatorio
		{
			::vel.linear.x = 0.0;
			::vel.angular.z = ::alea;
 
			vel_pub_.publish(::vel);
 
			loop_rate.sleep();
		}
	}
} 
 
 
int main(int argc, char **argv)
{
 
	ros::init(argc, argv, "juguete_golpe");   //Creación del nodo "juguete_golpe"
 
	ros::NodeHandle n;
 
  	ros::Publisher vel_pub_=n.advertise<geometry_msgs::Twist>("cmd_vel", 1);   //Publicación del topic "cmd_vel"
 
  	ros::Subscriber odom_sub_= n.subscribe("turtlebot_node/sensor_state", 1, callback);  //Suscripción del topic "turtlebot_node/sensor_state"
 
	ros::spin();   //Mantiene la suscripción al topic hasta que se reciba "Ctrl+C"
 
        return 0;
}
 
